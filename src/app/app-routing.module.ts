import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsTestingComponent } from './forms-testing/forms-testing.component';


const routes: Routes = [
  { path: '', component: FormsTestingComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
