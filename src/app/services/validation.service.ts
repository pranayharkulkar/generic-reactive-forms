import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  validationText = [];
  constructor() { }

  validate_form(formMembers, inputForm) {
    this.validationText = [];
    formMembers.forEach( elem => {
      // let field_name = elem.field;
      elem.validators.forEach( val => {
          this[val.name](elem, inputForm);
      });
    });
    console.log(this.validationText);
    return this.validationText.length ? this.validationText[0] : 'Valid form';
  }

  required(data, inputForm) {
    const field_name = inputForm.value[data.field];
    if (!field_name) {
      this.validationText.push(data.validators[0].text);
    }
  }

  pattern(data, inputForm) {
    const pattern = data.validators[1].value;
    const field_name = inputForm.value[data.field];
    if (new RegExp(`${pattern}`).test(field_name)) {
      return true;
    } else {
      this.validationText.push(data.validators[1].text);
      return false;
    }
  }
}
