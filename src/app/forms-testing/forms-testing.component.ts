import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ValidationService } from '../services/validation.service';
import * as  data from '../../assets/sampleForm.json';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forms-testing',
  templateUrl: './forms-testing.component.html',
  styleUrls: ['./forms-testing.component.scss']
})
export class FormsTestingComponent implements OnInit {
  sampleForm: FormGroup;
  members = data.form;

  constructor(private fb: FormBuilder, private validationService: ValidationService) { }

  ngOnInit() {
    const fields = {};
    this.members.forEach((elem) => {
        console.log(elem.field);
        if (!elem.isArray) {
          fields[elem.field] = [];
        } else {
          console.log('will push it here');
        }
    });
    console.log(fields);
    this.sampleForm = this.fb.group(fields);
    console.log(this.sampleForm);
  }

  submit() {
    console.log(this.sampleForm.value);
    const validStatus = this.validationService.validate_form(this.members, this.sampleForm);
    console.log(validStatus);
    Swal.fire(validStatus);
  }


}
